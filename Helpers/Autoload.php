<?php

function __autoload($class){
	$folders = array(
		"Controllers",
		"Helpers",
		"Models",
		"Models/Repositories",
		"Views"
	);
	foreach($folders as $folder){
		$path = "$folder/$class.php";
		if(file_exists($path)){
			require $path;
		}
	}
}