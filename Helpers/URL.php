<?php

Class URL {

	public static function generate($controller,$action,$identifier,$additionalParameters){
		$configuration = require "Configuration/Configuration.php";
		$URL = "";
		if($configuration["GENERAL"]["ABSOLUTE_PATH"]){
			$URL .= $configuration["GENERAL"]["DOMAIN"];
			if($URL[strlen($URL)-1]!="/"){
				$URL .= "/";
			}
		}
		if($configuration["GENERAL"]["REWRITE_MODE"]){
			$routesRepository = new RoutesRepository();
			// TO DO: add additional parameters to routes
			$route = $routesRepository->check($controller,$action,$identifier);
			if($route){
				$URL .= $route->getSource();
			} else {
				$URL .= "$controller/$action";
				if($identifier){
					$URL .= "/$identifier";
				}
				foreach($additionalParameters as $key => $value){
					$URL .= "/$key/$value";
				}
			}
		} else {
			$URL .= "index.php?C=$controller&A=$action";
			if($identifier){
				$URL .= "&ID=$identifier";
			}
			foreach($additionalParameters as $key => $value){
				$URL .= "&$key=$value";
			}
		}
		return $URL;
	}

	public static function show($controller,$action,$identifier = NULL,$additionalParameters = array()){
		echo self::generate($controller,$action,$identifier,$additionalParameters);
	}

	public static function redirect($controller,$action,$identifier = NULL,$additionalParameters = array()){
		header("Location: ".self::generate($controller,$action,$identifier,$additionalParameters));
	}

}