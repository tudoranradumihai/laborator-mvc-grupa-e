<?php

Class MVC {

	private $controller;
	private $action;
	private $object;

	public function getRequest(){
		if(array_key_exists("REQUEST", $_GET)){
			$object = RoutesController::check($_GET["REQUEST"]);
			if(is_object($object)){
				$_GET["C"] = $object->getController();
				$_GET["A"] = $object->getAction();
				$_GET["ID"] = $object->getIdentifier();
			} else {
				$request = explode("/",$_GET["REQUEST"]);
				if(array_key_exists(0, $request)){
					$_GET["C"] = $request[0];
				}
				if(array_key_exists(1, $request)){
					$_GET["A"] = $request[1];
				}
				if(array_key_exists(2, $request)){
					if(is_numeric($request[2])){
						$_GET["ID"] = $request[2];
					} else {
						if(array_key_exists(3, $request)){
							$_GET[$request[2]] = $request[3];
						}
					}
				}
			}	
		}
		$configuration = require "Configuration/Configuration.php";
		if(array_key_exists("C", $_GET)){
			$this->controller = ucfirst($_GET["C"])."Controller";
		} else {
			if(!empty($configuration["DEFAULT"]["CONTROLLER"])){
				$this->controller = $configuration["DEFAULT"]["CONTROLLER"];
			} else {
				$this->controller = "DefaultController";
			}
		}
		if(array_key_exists("A", $_GET)){
			$this->action = lcfirst($_GET["A"])."Action";
		} else {
			if(!empty($configuration["DEFAULT"]["ACTION"])){
				$this->action = $configuration["DEFAULT"]["ACTION"];
			} else {
				$this->action = "defaultAction";
			}
		}
	}

	public function prepare(){
		self::getRequest();

		if(class_exists($this->controller)){
			$this->object = new $this->controller();
			if(method_exists($this->object, $this->action)){
				$accessConfiguration = require "Configuration/AccessConfiguration.php";
				$access = true;
				if(array_key_exists($this->controller, $accessConfiguration)){
					if(in_array($this->action, $accessConfiguration[$this->controller])){
						if(array_key_exists("user", $_COOKIE)){
							$user = unserialize($_SESSION["user"]); 
							if(!$user->administrator){
								$access = false;
							}
						} else {
							$access = false;
						}
					}
				}
				if($access){
					$layoutConfiguration = require "Configuration/LayoutConfiguration.php";
					if(array_key_exists($this->controller, $layoutConfiguration)){
						if(array_key_exists($this->action, $layoutConfiguration[$this->controller])){
							$layout = $layoutConfiguration[$this->controller][$this->action]."Layout";
						} else {
							$layout = "DefaultLayout";
						}
					} else {
						$layout = "DefaultLayout";
					}
					
				} else {
					$error = "ERROR: cannot access $this->controller::$this->action.";
					LOG::write($error);
					$layout = "Error404Layout";
				}
			} else {
				$error = "ERROR: method $this->controller::$this->action does not exists.";
				LOG::write($error);
				$layout = "Error404Layout";
			}
		} else {
			$error = "ERROR: class $this->controller does not exists.";
			LOG::write($error);
			$layout = "Error404Layout";
		}
		require "Resources/Layouts/$layout.php";
	}

	public function render(){
		$this->object->{$this->action}();
	}

}