<?php

Class Mail {

	private $headers;

	public function __construct($from, $replyTo=NULL){
		if(is_null($replyTo)){
			$replyTo = $from;
		}
		$this->headers = 'From: $from' . "\r\n" .
		    'Reply-To: $replyTo' . "\r\n" .
		    'X-Mailer: PHP/' . phpversion();
	}

	public function send($to,$subject,$body){
		mail($to,$subject,$body,$this->headers);
	}

}