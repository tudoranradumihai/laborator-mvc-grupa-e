<?php

Class LOG {

	public static function write($log){
		$file = fopen("error_log","a+");
		fputs($file,date("Y-m-d H:i:s")." | ".$log.PHP_EOL);
		fclose($file);
	}

}