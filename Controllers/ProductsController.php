<?php 

Class ProductsController extends Controller {

	private $productsRepository;

	public function __construct(){
		parent::__construct();
		$this->productsRepository = new ProductsRepository();
	}

	public function defaultAction(){
		self::listAction();
	}

	public function latestAction(){
		$products = $this->productsRepository->findAll();
		$this->view->add("products",$products);
		$this->view->render(
			array(
				"test"=>"TEST"
			)
		);
	}

	public function listAction(){
		$products = $this->productsRepository->findAll();
		include "Views/Products/list.php";
	}

	public function listByFilterAction(){
		if(array_key_exists("search", $_GET)){
			if(!empty(trim($_GET["search"]))){
				$products = $this->productsRepository->findByNameOrDescription($_GET["search"]);
			} else {
				$products = $this->productsRepository->findAll();
			}
		} else {
			$products = $this->productsRepository->findAll();
		}
		
		$this->view->render(
			array(
				"products" => $products
			)
		);
	}

	public function listByCategoryAction(){
		if(array_key_exists("ID", $_GET)){
			$categoriesRepository = new CategoriesRepository();
			$category = $categoriesRepository->findByID($_GET["ID"]);
			$products = $this->productsRepository->findByColumn("category",$_GET["ID"]);
			$this->view->render(
			array(
				"category" => $category,
				"products" => $products
			)
		);
		} else {
			URL::redirect("Categories","list");
		}	
	}

	public function showAction(){
		if(array_key_exists("ID", $_GET)){ 
			$product = $this->productsRepository->findByID($_GET["ID"]);
			$this->view->render(
				array(
					"product" => $product
				)
			);	
		} else {
			URL::redirect("Products","list");
		}
	}

	public function newAction(){
		$categoriesRepository = new CategoriesRepository();
		$categories = $categoriesRepository->findAll();
		$this->view->render(array(
				"categories" => $categories
			)
		);	
	}

	public function createAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$validation = true;
			// TO DO: Validations
			if($validation){
				if($_FILES["image"]["size"]>0){
					$extension = explode(".",$_FILES["image"]["name"]);
					$extension = $extension[count($extension)-1];
					$filename = date("YmdHis")."-".rand(10000,99999).".".$extension;
					if(!file_exists("Resources")){
						mkdir("Resources");
					}
					if(!file_exists("Resources/Uploads")){
						mkdir("Resources/Uploads");
					}
					if(!file_exists("Resources/Uploads/FileReferences")){
						mkdir("Resources/Uploads/FileReferences");
					}
					if(move_uploaded_file($_FILES["image"]["tmp_name"], "Resources/Uploads/FileReferences/".$filename)){
						$fileReference = new FileReferences();
						$fileReference->setPath($filename);
						$fileReferencesRepository = new FileReferencesRepository();
						$result = $fileReferencesRepository->insert($fileReference);
						if($result){
							$fileID = $fileReferencesRepository->lastInsert();
						} else {
							$validation = false;
						}
					} else {
						$validation = false;
					}
				}
			}

			if($validation){
				$product = new Products();
				$product->setName($_POST["name"]);
				$product->setDescription($_POST["description"]);
				$product->setUrl($_POST["url"]);
				$product->setPrice($_POST["price"]);
				$product->setStock($_POST["stock"]);
				$product->setCategory($_POST["category"]);
				if(isset($fileID)){
					$product->setImage($fileID);
				}
				//$product->setUrl($_POST["url"]);
				$result = $this->productsRepository->insert($product);	
				
				$productID = $this->productsRepository->lastInsert();
				$route = new Routes();
				$route->setSource($_POST["url"]);
				$route->setController("Products");
				$route->setAction("show");
				$route->setIdentifier($productID);
				$routesRepository = new RoutesRepository();
				$result = $routesRepository->insert($route);

				include "Views/Products/create.php";
			} else {
				URL::redirect("Products","new");
			}
		} else {
			URL::redirect("Products","new");
		}
	}

	public function editAction(){
		if(array_key_exists("ID", $_GET)){
			$product = $this->productsRepository->findByID($_GET["ID"]);
			if(array_key_exists("edit-product", $_SESSION)){
				$status = $_SESSION["edit-product"]; 
				unset($_SESSION["edit-product"]);
			}
			require "Views/Products/edit.php";
		} else {
			URL::redirect("Products","list");	
		}
			
	}

	public function updateAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$product = new Products();
			foreach($product as $property => $value){
				if(array_key_exists($property, $_POST)){
					$product->$property = $_POST[$property];
				}
			}
			$result = $this->productsRepository->update($product);
			$_SESSION["edit-product"] = $result;
			URL::redirect("Products","edit",$product->id);
		} else {
			URL::redirect("Products","list");
		}
	}

	public function deleteAction(){
		if(array_key_exists("ID", $_GET)){
			$this->productsRepository->delete($_GET["ID"]);		
		} 
		URL::redirect("Products","list");
	}

}