<?php

Class UsersController extends Controller {

	private $usersRepository;

	public function __construct(){
		$this->usersRepository = new UsersRepository();
	}

	public function defaultAction(){

	}

	public function listAction(){
		$users = $this->usersRepository->findAll();
		include "Views/Users/list.php";
	}

	public function newAction(){
		include "Views/Users/new.php";
	}

	public function createAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$validation = true;
			if($validation){
				$user = new Users();
				$user->setFirstname($_POST["firstname"]);
				$user->setLastname($_POST["lastname"]);
				$user->setEmail($_POST["email"]);
				$user->setPassword(md5($_POST["password"]));
				$result = $this->usersRepository->insert($user);
				if($result){
					$userID = $this->usersRepository->lastInsert();
					$usersConfirmation = new UsersConfirmation();
					$usersConfirmation->setUser($userID);
					$hash = md5(date("YmdHis").rand(10000,99999).str_pad($userID,10,"0",STR_PAD_LEFT));
					$usersConfirmation->setHash($hash);
					$usersConfirmationRepository = new UsersConfirmationRepository();
					$result = $usersConfirmationRepository->insert($usersConfirmation);
					if($result){
						$confirmationLink = URL::generate("UsersConfirmation","confirm",NULL,array("hash"=>$hash));
						$mail = new Mail("office@myshop.com","no-reply@myshop.com");
						$body = "Salut $_POST[firstname],\n
						\n
						Apasa aici si nu vei regreta!\n
						<a target=\"_blank\" href=\"$confirmationLink\">$confirmationLink</a>\n
						Sau\n
						<input value=\"$confirmationLink\">";
						$mail->send($_POST["email"],"Confirmare Cont",$body);
						echo nl2br($body);
					} else {
						/* ERROR */
					}
				} else {
					/* ERROR */
				}
			} else {
				URL::redirect("Users","new");
			}
		} else {
			URL::redirect("Users","new");
		}
	}

	public function loginAction(){
		if(!array_key_exists("user", $_COOKIE)){
			require "Views/Users/login.php";
		} else {
			URL::redirect("Users","welcome");
		}
	}

	public function connectAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$user = $this->usersRepository->checkCredentials($_POST["email"],$_POST["password"]);
			if(is_object($user)){
				$configuration = require "Configuration/Configuration.php";
				setcookie("user",$user->getId(),time()+3600,"/",preg_replace("#^https?://#","",$configuration["GENERAL"]["DOMAIN"]));
				$user->setPassword("");
				$_SESSION["user"] = serialize($user);
				URL::redirect("Users","welcome");
			} else {
				// TO DO: SESSION ERRORS
				URL::redirect("Users","login");	
			}
		} else {
			URL::redirect("Users","login");
		}
	}

	public function disconnectAction(){
		if(array_key_exists("user", $_COOKIE)){
			$configuration = require "Configuration/Configuration.php";
			setcookie("user",NULL,time()-3600,"/",preg_replace("#^https?://#","",$configuration["GENERAL"]["DOMAIN"]));
		}
		if(array_key_exists("user", $_SESSION)){
			unset($_SESSION["user"]);
		}
		if(array_key_exists("HTTP_REFERER", $_SERVER)){
			header("Location: $_SERVER[HTTP_REFERER]");
		} else {
			URL::redirect("Users","login");
		}
	}

	public function welcomeAction(){
		if(array_key_exists("user", $_COOKIE)){
			$user = $this->usersRepository->findByID($_COOKIE["user"]);
			require "Views/Users/welcome.php";
		} else {
			URL::redirect("Users","login");
		}
	}

	public function administrationAction(){
		require "Views/Users/administration.php";
	}


	public function editAction(){
		require "Views/Users/edit.php";	
	}



	public static function loginStatus(){
		if(array_key_exists("user", $_COOKIE)){
			$status = true;
			if(array_key_exists("user", $_SESSION)){
				$user = unserialize($_SESSION["user"]); 	
			} else {
				$usersRepository = new UsersRepository();
				$user = $usersRepository->findByID($_COOKIE["user"]);
				$_SESSION["user"] = serialize($user);
			}
		} else {
			$status = false;
		}
		require "Views/Users/loginStatus.php";
	}

}