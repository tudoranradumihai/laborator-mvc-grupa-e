<?php

Class CategoriesController extends Controller {

	private $categoriesRepository;

	public function __construct(){
		$this->categoriesRepository = new CategoriesRepository();
	}

	public function defaultAction(){

	}

	public function listAction(){
		$categories = $this->categoriesRepository->findAll();
		include "Views/Categories/list.php";
	}

}