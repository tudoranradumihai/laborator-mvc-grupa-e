<?php

Class CartsController extends Controller {

	private $cartsRepository;

	public function __construct(){
		$this->cartsRepository = new CartsRepository();
	}

	public function defaultAction(){
		self::listAction();
	}

	public function listAction(){
		$cartItems = array();
		$priceTotal = 0;
		$cartItemsRepository = new CartItemsRepository();
		$productsRepository = new ProductsRepository();
		if(array_key_exists("user", $_COOKIE)){
			$cart = $this->cartsRepository->findByColumn("user",$_COOKIE["user"],FALSE,"DESC");
			if(is_array($cart)){
				$cart = reset($cart);
				$cartItems = $cartItemsRepository->findByColumn("cart",$cart->getId(),FALSE);
				foreach($cartItems as $key => $cartItem){
					$product = $productsRepository->findById($cartItem->getProduct());
					$cartItem->setProduct($product);
					$priceTotal += $cartItem->price = $cartItem->getQuantity()*$product->getPrice();
					$cartItems[$key] = $cartItem;
				}
			}
		} else {
			$cart = $this->cartsRepository->findByColumn("session",session_id(),TRUE,"DESC");
			if(is_array($cart)){
				$cart = reset($cart);
				$cartItems = $cartItemsRepository->findByColumn("cart",$cart->getId(),FALSE);
				foreach($cartItems as $key => $cartItem){
					$product = $productsRepository->findById($cartItem->getProduct());
					$cartItem->setProduct($product);
					$priceTotal += $cartItem->price = $cartItem->getQuantity()*$product->getPrice();
					$cartItems[$key] = $cartItem;
				}
			}
		}
		require "Views/Carts/list.php";
	}

	public static function status(){
		require "Views/Carts/status.php";
	}

	public static function newAction($product){
		require "Views/Carts/new.php";
	}

	public function createAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$cartItemsRepository = new CartItemsRepository();
			if(array_key_exists("user", $_COOKIE)){
				$cart = $this->cartsRepository->findByColumn("user",$_COOKIE["user"]);
				if(is_array($cart)){
					$cart = reset($cart);
				}
				if(!is_null($cart)){
					$cartItem = $cartItemsRepository->findByCartAndProduct($cart->getId(),$_POST["product"]);
					if(is_object($cartItem)){
						$cartItem->setQuantity(intval($cartItem->getQuantity())+intval($_POST["quantity"]));
						$result = $cartItemsRepository->update($cartItem);
					} else {
						$cartItem = new CartItems();
						$cartItem->setCart($cart->getId());
						$cartItem->setProduct($_POST["product"]);
						$cartItem->setQuantity($_POST["quantity"]);
						$result = $cartItemsRepository->insert($cartItem);
					}
				} else {
					$cart = $this->cartsRepository->findByColumn("session",session_id(),TRUE);
					if(is_array($cart)){
						$cart = reset($cart);
					}
					if(!is_null($cart)){
						$cart->setUser($_COOKIE["user"]);
						$result = $this->cartsRepository->update($cart);
						$cartItem = $cartItemsRepository->findByCartAndProduct($cart->getId(),$_POST["product"]);
						if(is_object($cartItem)){
							$cartItem->setQuantity(intval($cartItem->getQuantity())+intval($_POST["quantity"]));
							$result = $cartItemsRepository->update($cartItem);
						} else {
							$cartItem = new CartItems();
							$cartItem->setCart($cart->getId());
							$cartItem->setProduct($_POST["product"]);
							$cartItem->setQuantity($_POST["quantity"]);
							$result = $cartItemsRepository->insert($cartItem);
						}
					} else {
						$cart = new Carts();
						$cart->setUser($_COOKIE["user"]);
						$cart->setSession(session_id());
						$result = $this->cartsRepository->insert($cart);
						if($result){
							$cartID = $this->cartsRepository->lastInsert();
							$cartItem = new CartItems();
							$cartItem->setCart($cartID);
							$cartItem->setProduct($_POST["product"]);
							$cartItem->setQuantity($_POST["quantity"]);
							$result = $cartItemsRepository->insert($cartItem);
						}
					}
				}
			} else {
				$cart = $this->cartsRepository->findByColumn("session",session_id(),TRUE);
				if(is_array($cart)){
					$cart = reset($cart);
				}
				if(!is_null($cart)){
					$cartItem = $cartItemsRepository->findByCartAndProduct($cart->getId(),$_POST["product"]);
					if(is_object($cartItem)){
						$cartItem->setQuantity(intval($cartItem->getQuantity())+intval($_POST["quantity"]));
						$result = $cartItemsRepository->update($cartItem);
					} else {
						$cartItem = new CartItems();
						$cartItem->setCart($cart->getId());
						$cartItem->setProduct($_POST["product"]);
						$cartItem->setQuantity($_POST["quantity"]);
						$result = $cartItemsRepository->insert($cartItem);
					}
				} else {
					$cart = new Carts();
					$cart->setSession(session_id());
					$result = $this->cartsRepository->insert($cart);
					if($result){
						$cartID = $this->cartsRepository->lastInsert();
						$cartItem = new CartItems();
						$cartItem->setCart($cartID);
						$cartItem->setProduct($_POST["product"]);
						$cartItem->setQuantity($_POST["quantity"]);
						$result = $cartItemsRepository->insert($cartItem);
					}
				}
			}
		}
		URL::redirect("Carts","list");
	}

	public function updateAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$cartItemsRepository = new CartItemsRepository();
			foreach($_POST["items"] as $key => $value){
				$cartItem = $cartItemsRepository->findById($key);
				$cartItem->setQuantity($value);
				$cartItemsRepository->update($cartItem);
			}
		} 
		URL::redirect("Carts","list");
	}

}