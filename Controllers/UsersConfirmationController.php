<?php

Class UsersConfirmationController extends Controller {

	private $usersConfirmationRepository;
	private $usersRepository;

	public function __construct(){
		$this->usersConfirmationRepository = new UsersConfirmationRepository();
		$this->usersRepository = new UsersRepository();
	}

	public function defaultAction(){
		/* ... */
	}

	public function confirmAction(){
		date_default_timezone_set("Europe/Bucharest");
		if(array_key_exists("hash", $_GET)){
			$result = $this->usersConfirmationRepository->findByColumn("hash",$_GET["hash"],TRUE);
			$validation = false;
			if(is_array($result)){
				$usersConfirmation = reset($result);
				$usersConfirmation->setDeleted(1);
				$createdDate = strtotime($usersConfirmation->getCreateddate());
				$expiredDate = $createdDate+7200;
				if($expiredDate<time()){
					$result = $this->usersRepository->delete($usersConfirmation->getUser());
					$message = "ERROR";
				} else {
					$result = $this->usersRepository->unhide($usersConfirmation->getUser());
					$validation = true;
					$message = "SUCCESS";
				}
				$this->usersConfirmationRepository->update($usersConfirmation);
			} else {
				$message = "ERROR";
			}
			require "Views/UsersConfirmation/confirm.php";
		} else {
			URL::redirect("Users","new");
		}
	}

	public function cronAction(){
		$usersConfirmations = $this->usersConfirmationRepository->findAllExpired();
		foreach($usersConfirmations as $usersConfirmation){
			$this->usersConfirmationRepository->delete($usersConfirmation->getId());
			$this->usersRepository->delete($usersConfirmation->getUser());
		}
	}

}