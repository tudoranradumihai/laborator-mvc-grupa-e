<?php

Class RoutesController extends Controller {

	private $routesRepository;

	public function __construct(){
		$this->routesRepository = new RoutesRepository();
	}

	public function defaultAction(){
		self::listAction();
	}

	public function newAction(){
		$routesConfiguration = require "Configuration/RoutesConfiguration.php";
		$controllers = array();
		foreach ($routesConfiguration as $key => $value) {
			array_push($controllers,preg_replace("/Controller$/","",$key));
		}
		require "Views/Routes/new.php";
	}

	public function getClassMethodsAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$routesConfiguration = require "Configuration/RoutesConfiguration.php";
			$methods = array();
			if(array_key_exists($_POST["controller"]."Controller", $routesConfiguration)){
				foreach($routesConfiguration[$_POST["controller"]."Controller"] as $value){
					array_push($methods,preg_replace("/Action$/","",$value));
				}
			}
			echo json_encode($methods);
		}	
	}

	public function createAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$route = new Routes();
			if($_POST["type"]=="direct"){
				$route->setSource($_POST["source"]);
				$route->setTarget($_POST["target"]);
			} else {
				$route->setSource($_POST["source"]);
				$route->setController($_POST["controller"]);
				$route->setAction($_POST["action"]);
				$route->setIdentifier($_POST["identifier"]);
			}
			$result = $this->routesRepository->insert($route);
		} else {
			URL::redirect("Routes","new");
		}
	}

	public static function check($source){
		$routesRepository = new RoutesRepository();
		$route = $routesRepository->findByColumn("source",$source,TRUE);
		if(is_array($route)){
			return array_pop($route);
		} else {
			return NULL;
		}
	} 

	public function listAction(){

	}

}