<?php

Abstract Class Controller {

	protected $view;

	abstract public function defaultAction();

	public function __construct(){
		$this->view = new View();
	}

}