<?php
return array(
	"CategoriesController" => array(
		"listAction",
		"showAction"
	),
	"ProductsController" => array(
		"listAction",
		"listByCategoryAction",
		"showAction"
	)
);