<?php
return array(
	"GENERAL" => array(
		"DOMAIN" => "http://myshop.com",
		"ABSOLUTE_PATH" => true,
		"REWRITE_MODE" => true
	),
	"DEFAULT" => array(
		"CONTROLLER" => "ProductsController",
		"ACTION" => "latestAction"
	),
	"DATABASE" => array(
		"HOSTNAME" => "localhost",
		"USERNAME" => "root",
		"PASSWORD" => "",
		"DATABASE" => "mvce"
	)
);