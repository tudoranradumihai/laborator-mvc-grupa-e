<?php
return array(
	"CategoriesController" => array(
		"newAction",
		"createAction",
		"editAction",
		"updateAction",
		"deleteAction"
	),
	"ProductsController" => array(
		"newAction",
		"createAction",
		"editAction",
		"updateAction",
		"deleteAction"
	)
);