<?php

Class Products extends Model {
	
	private $id;
	private $name;
	private $description;
	private $url;
	private $price;
	private $stock;
	private $category;
	private $image;
	private $createddate;
	private $updateddate;

	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}

	public function getDescription(){
		return $this->description;
	}

	public function setDescription($description){
		$this->description = $description;
	}

	public function getUrl(){
		return $this->url;
	}

	public function setUrl($url){
		$this->url = $url;
	}

	public function getPrice(){
		return $this->price;
	}

	public function setPrice($price){
		$this->price = $price;
	}

	public function getStock(){
		return $this->stock;
	}

	public function setStock($stock){
		$this->stock = $stock;
	}

	public function getCategory(){
		return $this->category;
	}

	public function setCategory($category){
		$this->category = $category;
	}

	public function getImage(){
		return $this->image;
	}

	public function getImageObject(){
		if($this->image){
			$fileReferencesRepository = new FileReferencesRepository();
			$image = $fileReferencesRepository->findById($this->image);
			return $image;
		} else {
			return NULL;
		}
	}

	public function setImage($image){
		$this->image = $image;
	}
	
	public function getCreateddate(){
		return $this->createddate;
	}

	public function setCreateddate($createddate){
		$this->createddate = $createddate;
	}

	public function getUpdateddate(){
		return $this->updateddate;
	}

	public function setUpdateddate($updateddate){
		$this->updateddate = $updateddate;
	}


}