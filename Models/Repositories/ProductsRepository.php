<?php

Class ProductsRepository extends Repository {

	public function findByNameOrDescription($text){
		$query = "SELECT * FROM $this->table WHERE name LIKE '%$text%' OR description LIKE '%$text%';";
		$result = $this->database->query($query);
		$objects = array();
		if (is_object($result)){
			if($result->num_rows > 0){
				while($item = $result->fetch_assoc()){
					$object = new $this->model();
					foreach($object->getProperties() as $property){
						$object->{"set".str_replace(" ","",ucwords(str_replace("_"," ",$property)))}($item[$property]);
					}
					array_push($objects, $object);
				}
			}
		}
		return $objects;
	}

}