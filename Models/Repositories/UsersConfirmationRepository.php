<?php

Class UsersConfirmationRepository extends Repository {

	public function findAllExpired(){
		date_default_timezone_set("Europe/Bucharest");
		$query = "SELECT * FROM $this->table WHERE deleted=0 AND createddate > '".date("Y-m-d H:i:s",(time()-7200))."';";
		$result = $this->database->query($query);
		$objects = array();
		if (is_object($result)){
			if($result->num_rows > 0){
				while($item = $result->fetch_assoc()){
					$object = new $this->model();
					foreach($object->getProperties() as $property){
						$object->{"set".str_replace(" ","",ucwords(str_replace("_"," ",$property)))}($item[$property]);
					}
					array_push($objects, $object);
				}
			}
		}
		return $objects;
	} 

}