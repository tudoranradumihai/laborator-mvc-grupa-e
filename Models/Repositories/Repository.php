<?php

Abstract Class Repository {

	protected $database;
	protected $model;
	protected $table;

	public function __construct(){
		$this->database = new Database();
		$this->model = preg_replace('/Repository$/','',get_called_class());
		$this->table = strtolower(implode("_",preg_split('/(?=[A-Z])/',lcfirst(preg_replace('/Repository$/','',get_called_class())))));
	}

	public function lastInsert(){
		return $this->database->lastInsert();
	}

	public function findAll(){
		$query = "SELECT * FROM $this->table";
		$result = $this->database->query($query);
		$objects = array();
		if (is_object($result)){
			if($result->num_rows > 0){
				while($item = $result->fetch_assoc()){
					$object = new $this->model();
					foreach($object->getProperties() as $property){
						$object->{"set".str_replace(" ","",ucwords(str_replace("_"," ",$property)))}($item[$property]);
					}
					array_push($objects, $object);
				}
			}
		}
		return $objects;
	}


	public function findByID($identifier){
		$query = "SELECT * FROM $this->table WHERE id=$identifier";
		$result = $this->database->query($query);
		if($result->num_rows > 0){
			$item = $result->fetch_assoc();
			$object = new $this->model();
			foreach($object->getProperties() as $property){
				$object->{"set".str_replace(" ","",ucwords(str_replace("_"," ",$property)))}($item[$property]);
			}
			return $object;
		} else {
			return NULL;
		}	
	}

	public function findByColumn($column,$value,$string=FALSE,$order="ASC"){
		if($string){
			$query = "SELECT * FROM $this->table WHERE $column LIKE '$value' ORDER BY createddate $order";
		} else {
			$query = "SELECT * FROM $this->table WHERE $column=$value ORDER BY createddate $order";	
		}
		$result = $this->database->query($query);
		$objects = array();
		if (is_object($result)){
			if($result->num_rows > 0){
				while($item = $result->fetch_assoc()){
					$object = new $this->model();
					foreach($object->getProperties() as $property){
						$object->{"set".str_replace(" ","",ucwords(str_replace("_"," ",$property)))}($item[$property]);
					}
					array_push($objects, $object);
				}
			}
		}
		if(count($objects)>0){
			return $objects;
		} else {
			return NULL;
		}
	}	

	public function insert($object){
		$properties = array();
		$values = array();
		foreach ($object->getProperties() as $property){
			if($object->{"get".ucfirst($property)}()!=""){
				array_push($properties,$property);
				array_push($values,'"'.$object->{"get".ucfirst($property)}().'"');
			}
		}
		$query = "INSERT INTO $this->table (".implode(",",$properties).") VALUES (".implode(",",$values).");";
		return $this->database->query($query);
	}

	public function update($object){
		$values = array();
		foreach($object->getProperties() as $property){
			if($object->{"get".ucfirst($property)}()!==NULL && $property!="id"){
				array_push($values,"$property='".$object->{"get".ucfirst($property)}()."'");
			}
		}

		$query = "UPDATE $this->table SET ".implode(",",$values)." WHERE id=".$object->getId();
		return $this->database->query($query);
	}

	public function delete($identifier){
		//$query = "DELETE FROM $this->table WHERE id=$identifier";
		$query = "UPDATE $this->table SET deleted=1 WHERE id=$identifier";
		return $this->database->query($query);
	}

	public function hide($identifier){
		$query = "UPDATE $this->table SET hidden=1 WHERE id=$identifier";
		return $this->database->query($query);
	}

	public function unhide($identifier){
		$query = "UPDATE $this->table SET hidden=0 WHERE id=$identifier";
		return $this->database->query($query);
	}

}