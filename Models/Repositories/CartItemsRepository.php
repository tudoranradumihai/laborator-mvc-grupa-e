<?php

Class CartItemsRepository extends Repository {

	public function findByCartAndProduct($cart,$product){
		$query = "SELECT * FROM $this->table WHERE cart=$cart AND product=$product";
		$result = $this->database->query($query);
		if($result->num_rows > 0){
			$item = $result->fetch_assoc();
			$object = new $this->model();
			foreach($object->getProperties() as $property){
				$object->{"set".str_replace(" ","",ucwords(str_replace("_"," ",$property)))}($item[$property]);
			}
			return $object;
		} else {
			return NULL;
		}	
	}
	
}