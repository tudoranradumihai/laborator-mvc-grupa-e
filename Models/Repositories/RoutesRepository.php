<?php

Class RoutesRepository extends Repository {
	
	public function check($controller,$action,$identifier = NULL){
		$query = "SELECT * FROM $this->table WHERE controller LIKE '$controller' AND action LIKE '$action'";
		if(!is_null($identifier)){
			$query .= " AND identifier = $identifier;";
		}
		$result = $this->database->query($query);
		if($result->num_rows > 0){
			$item = $result->fetch_assoc();
			$object = new $this->model();
			foreach($object->getProperties() as $property){
				$object->{"set".str_replace(" ","",ucwords(str_replace("_"," ",$property)))}($item[$property]);
			}
			return $object;
		} else {
			return NULL;
		}	
	}

}