<?php

Abstract Class Model {

	public function getProperties(){
		$reflect = new ReflectionClass($this);
		$reflectProperties = $reflect->getProperties();
		$properties = array();
		foreach($reflectProperties as $property){
			array_push($properties,$property->name);
		}
		return $properties;
	}
}