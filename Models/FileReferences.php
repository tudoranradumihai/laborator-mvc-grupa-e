<?php

Class FileReferences extends Model {

	private $id;
	private $name;
	private $path;

	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	} 

	public function getPath(){
		return $this->path;
	}

	public function setPath($path){
		$this->path = $path;
	} 

}