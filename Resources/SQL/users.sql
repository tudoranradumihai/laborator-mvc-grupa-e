CREATE TABLE users (
    id         INT(11) PRIMARY KEY AUTO_INCREMENT,

    deleted   INT(11) DEFAULT 0,
    hidden    INT(11) DEFAULT 1,  

    firstname  VARCHAR(255) NOT NULL,
    lastname   VARCHAR(255) NOT NULL,
    email      VARCHAR(255) NOT NULL UNIQUE,
    password   VARCHAR(255) NOT NULL,

    administrator INT(11) DEFAULT 0,

    createddate DATETIME DEFAULT CURRENT_TIMESTAMP,
    updateddate DATETIME ON UPDATE CURRENT_TIMESTAMP
);

/*
  ALTER TABLE users
    ADD COLUMN administrator INT(11) DEFAULT 0 AFTER password;
  ALTER TABLE users
    ADD COLUMN deleted INT(11) DEFAULT 0 AFTER id;
  ALTER TABLE users
    ADD COLUMN hidden INT(11) DEFAULT 1 AFTER deleted;
*/

	INSERT INTO users 
		(firstname,lastname,email,password)
		VALUES
		("John","Doe","john.doe@domain.com","81dc9bdb52d04dc20036dbd8313ed055"); /* password: 1234 */