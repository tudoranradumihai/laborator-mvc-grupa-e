CREATE TABLE cart_items (
	id         INT(11) PRIMARY KEY AUTO_INCREMENT,

	cart       INT(11) NOT NULL,
	product    INT(11) NOT NULL,
	quantity    INT(11) NOT NULL,

	createddate DATETIME DEFAULT CURRENT_TIMESTAMP,
	updateddate DATETIME ON UPDATE CURRENT_TIMESTAMP
);

/* TO DO FK for cart, product */