CREATE TABLE users_confirmation (
    id         INT(11) PRIMARY KEY AUTO_INCREMENT,

    deleted   INT(11) DEFAULT 0,

    user INT(11) NOT NULL,
    hash VARCHAR(255),

    createddate DATETIME DEFAULT CURRENT_TIMESTAMP,
    updateddate DATETIME ON UPDATE CURRENT_TIMESTAMP
);