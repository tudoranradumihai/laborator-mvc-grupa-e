CREATE TABLE products (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,

	name VARCHAR(255) NOT NULL,
	description TEXT,
	url VARCHAR(255),
	category INT(11),
	price FLOAT(9,2) NOT NULL,
	stock INT(11) DEFAULT 0,
	image INT(11) DEFAULT NULL,
	
	createddate DATETIME DEFAULT CURRENT_TIMESTAMP,
	updateddate DATETIME ON UPDATE CURRENT_TIMESTAMP
);

/*
  ALTER TABLE products
    ADD COLUMN category INT(11) AFTER description;

  ALTER TABLE products
    ADD COLUMN image INT(11) AFTER stock;

    ALTER TABLE products
    ADD COLUMN url VARCHAR(255) AFTER description;
*/
/* TO DO MAKE IT FOREIGN KEY */

INSERT INTO products (name,price)
	VALUES
		("Telefon mobil Apple iPhone 8 Plus, 64GB, 4G, Space Grey",4199.99),
		("Telefon mobil Samsung Galaxy S8 Plus, 64GB, 4G, Orchid Grey",3399.99);