<!DOCTYPE html>
<html>
	<head>
		<?php require "Resources/Partials/HeaderResources.php"; ?>
	</head>
	<body>
		<div class="container">
			<?php require "Resources/Partials/Header.php"; ?>
			<main>
				<?php self::render(); ?>
			</main>
			<?php require "Resources/Partials/Footer.php"; ?>
		</div>
		<?php require "Resources/Partials/FooterResources.php"; ?>
	</body>
</html>
