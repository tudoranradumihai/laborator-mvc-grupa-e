<!DOCTYPE html>
<html>
	<head>
		<?php require "Resources/Partials/HeaderResources.php"; ?>
	</head>
	<body>
		<div class="container">
			<main>
				<h1 class="text-center">404</h1>
				<h3><?php echo $error ?></h3>
			</main>
		</div>
		<?php require "Resources/Partials/FooterResources.php"; ?>
	</body>
</html>
