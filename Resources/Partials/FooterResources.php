<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
<script>
$(document).ready(function(){
	$("body").show();
	$("#type-1").on("click",function(){
		$(".container-type").hide();
		$("#container-type-1").show();
	});
	$("#type-2").on("click",function(){
		$(".container-type").hide();
		$("#container-type-2").show();
	});
	$("#controller").on("change",function(){
		$.ajax({
			url: "/api.php?C=Routes&A=getClassMethods",
			type: "POST",
			data: {
				controller: $(this).val()
			},
			success: function(data) {
				var array = $.parseJSON(data);
				if(array.length>0){
					$("#container-type-action").show();
					$("#action option").remove();
					$('#action').append($('<option>', { 
						value: "",
				        text : "..."
				    }));
					$.each(array, function( key, value ) {
						$('#action').append($('<option>', { 
					        text : value
					    }));
					});
				} else {
					$("#container-type-action").hide();
					$("#container-type-identifier").hide();
					$("#action option").remove();
				}
			}
		});
	});
	$("#action").on("change",function(){
		if($(this).val()!=""){
			$("#container-type-identifier").show();
		} else {
			$("#container-type-identifier").hide();
		}
	});

	$("#name").on("change",function(){
		var temporary = $(this).val().trim().toLowerCase().replace(/\ /g, '-');
		$("#url").val(temporary);
	});
});
</script>