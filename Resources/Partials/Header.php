<header>
	<div class="text-right">
		<?php UsersController::loginStatus(); ?>
	</div>
	<div class="text-right">
		<?php CartsController::status(); ?>
	</div>
	<h1>MVC <small>Model View Controller</small></h1>
	<form method="GET" action="<?php URL::show("Products","listByFilter");?>">
	<label>Search</label> 
	<input type="text" name="search" placeholder="your product here" value="<?php echo array_key_exists("search", $_GET) ? $_GET["search"] : "" ?>">
	<input type="submit" value="Search">
	</form>
</header>
<nav>
	<ul class="nav">
		<li class="nav-item">
			<a class="nav-link" href="/">Home</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="<?php URL::show("Categories","list"); ?>">Categories</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="<?php URL::show("Products","listByFilter"); ?>">All Products</a>
		</li>
	</ul>
</nav>