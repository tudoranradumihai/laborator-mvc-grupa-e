<ul>
	<?php foreach($categories as $category): ?>
		<li>
			<a href="<?php URL::show("Products","listByCategory",$category->id) ?>"><?php echo $category->name ?></a>
			<p><?php echo $category->description ?></p>
		</li>
	<?php endforeach; ?>
</ul>