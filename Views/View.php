<?php

Class View {

	private $variables = array();

	public function add($name,$value){
		$this->variables[$name] = $value;
	}

	public function render($variables = array()){
		$this->variables = array_merge($this->variables,$variables);
		unset($variables);
		$backtrace = debug_backtrace();
		if(array_key_exists(1, $backtrace)){
			$controller = preg_replace("/Controller$/","",$backtrace[1]["class"]);
			$action     = preg_replace("/Action$/","",$backtrace[1]["function"]);
			foreach($this->variables as $name => $value){
				$$name = $value;
			}
			unset($this->variables);
			require "Views/$controller/$action.php";
		} else {
			/* ERROR */
		}
	}

}