<h1>New Route</h1>
<form method="POST" action="<?php URL::show("Routes","create");?>">
<label for="source">Source</label><br>
<input type="text" name="source"><br>
<input type="radio" value="direct" id="type-1" name="type"> <label for="type-1">Direct Target</label><br>
<input type="radio" value="structured" id="type-2" name="type"> <label for="type-2">Controller & Action</label><br>
<div class="container-type" id="container-type-1">
<label for="Target">Target</label><br>
<input type="text" name="target"><br>
</div>
<div class="container-type" id="container-type-2">
	<div>
		<label for="controller">Controller</label><br>
		<select id="controller" name="controller">
			<option value="">...</option>
			<?php foreach($controllers as $item): ?>
				<option><?php echo $item ?></option>
			<?php endforeach; ?>
		</select>
	</div>
	<div id="container-type-action">
		<label for="action">Action</label><br>
		<select id="action" name="action">
		</select>
	</div>
	<div id="container-type-identifier">
		<label for="identifier">Identifier</label><br>
		<input id="identifier" name="identifier">
	</div>
</div>
<input type="submit">
</form>
<style>
.container-type, #container-type-action, #container-type-identifier {
	display:none;
}
</style>