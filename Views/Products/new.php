<div class="row">
	<div class="col-md-12">
		<h1>New Product</h1>
	</div>
	<div class="col-md-12">
	<form method="POST" action="<?php URL::show("Products","create"); ?>" enctype="multipart/form-data">
		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="name" class="col-form-label">Name</label>
				<input type="text" class="form-control" id="name" name="name" placeholder="Product Name">
			</div>
			<div class="form-group col-md-6">
				<label for="category" class="col-form-label">Category</label>
				<select class="form-control" name="category" id="category">
					<option value="">Select Category</option>
					<?php foreach($categories as $category): ?>
						<option class="dropdown-item" value="<?php echo $category->id; ?>"><?php echo $category->name; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="form-group col-md-12">
				<label for="url" class="col-form-label">Speaking URL</label>
				<input type="text" class="form-control" id="url" name="url" placeholder="Product URL">
			</div>
			<div class="form-group col-md-6">
				<label for="image">Image</label>
    			<input type="file" class="form-control-file" id="image" name="image">
			</div>
			<div class="form-group col-md-12">
				<label for="description" class="col-form-label">Description</label>
				<textarea class="form-control" id="description" name="description" rows="3"></textarea>
			</div>
			<div class="form-group col-md-6">
				<label for="price" class="col-form-label">Price</label>
				<input type="text" class="form-control" id="price" name="price" placeholder="99.99">
			</div>
			<div class="form-group col-md-6">
				<label for="stock" class="col-form-label">Stock</label>
				<input type="text" class="form-control" id="stock" name="stock" placeholder="0" >
			</div>
			<div class="form-group col-md-12 text-right">
				<button class="btn btn-primary" type="submit">Create Product</button>
			</div>
		</div>
	</form>
	</div>
</div>	