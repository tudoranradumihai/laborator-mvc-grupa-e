<div class="row">
	<div class="col-md-12">
		<h1>List <?php echo $category->name ?></h1>
		<p><?php echo $category->description ?></p>
	</div>
	<?php foreach ($products as $product): ?>
	<div class="col-md-6">
		<?php
		if($product->getImage()){
			$image = $product->getImageObject();
			$imagepath = "/Resources/Uploads/FileReferences/".$image->getPath();
		}
		?>
		<img src="<?php echo $imagepath; ?>" class="img-fluid">
		<a href="<?php URL::show("Products","show",$product->getId()); ?>">
			<h4 class="card-title"><?php echo $product->getName(); ?></h4>
		</a>
		<p class="card-text"><?php echo $product->getDescription(); ?></p>
		<h4 class="text-right"><?php echo $product->getPrice(); ?> RON</h4>
		<a href="#" class="btn btn-primary">Add to Cart</a>
		<a href="<?php URL::show("Products","edit",$product->getId()); ?>" class="btn btn-warning">Edit Product</a>
		<a href="<?php URL::show("Products","delete",$product->getId()); ?>" class="btn btn-danger">Delete Product</a>
	</div>
	<?php endforeach; ?>
</div>