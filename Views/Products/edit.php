<div class="row">
	<div class="col-md-12">
		<h1>Edit Product</h1>
	</div>
	<?php
	if(isset($status)){
		if($status){
			echo "numa bine rau la nime";
		} else {
			echo "NEM";
		}
	}
	?>
	<div class="col-md-12">
	<form method="POST" action="<?php URL::show("Products","update"); ?>">
		<input type="hidden" name="id" value="<?php echo $product->id ?>" />
		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="name" class="col-form-label">Name</label>
				<input type="text" class="form-control" id="name" name="name" placeholder="Product Name" value="<?php echo $product->name ?>">
			</div>
			<div class="form-group col-md-12">
				<label for="description" class="col-form-label">Description</label>
				<textarea class="form-control" id="description" name="description" rows="3"><?php echo $product->description; ?></textarea>
			</div>
			<div class="form-group col-md-6">
				<label for="price" class="col-form-label">Price</label>
				<input type="text" class="form-control" id="price" name="price" placeholder="99.99" value="<?php echo $product->price; ?>">
			</div>
			<div class="form-group col-md-6">
				<label for="stock" class="col-form-label">Stock</label>
				<input type="text" class="form-control" id="stock" name="stock" placeholder="0" value="<?php echo $product->stock; ?>">
			</div>
			<div class="form-group col-md-12 text-right">
				<button class="btn btn-primary"  type="submit">Edit Product</button>
			</div>
		</div>
	</form>
	</div>
</div>