<div class="row">
	<div class="col-md-12">
		<h1>List Products</h1>
	</div>
	<?php foreach ($products as $product): ?>
	<div class="col-md-3">
		<?php
		if($product->getImage()){
			$image = $product->getImageObject();
			$imagepath = "/Resources/Uploads/FileReferences/".$image->getPath();
			echo '<img src="'.$imagepath.'" class="img-fluid">';
		} else {
			echo '<img src="http://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg" class="img-fluid">';
		}
		?>
		<a href="<?php URL::show("Products","show",$product->getId()); ?>">
			<h4 class="card-title"><?php echo $product->getName(); ?></h4>
		</a>
		<p class="card-text"><?php echo $product->getDescription(); ?></p>
		<h4 class="text-right"><?php echo $product->getPrice(); ?> RON</h4>
	</div>
	<?php endforeach; ?>
</div>