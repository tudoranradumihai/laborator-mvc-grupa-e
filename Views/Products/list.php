<div class="row">
	<div class="col-md-9">
		<h1>Products</h1>
		<p>Lorem Ipsum ...</p>
	</div>
	<div class="col-md-3 text-right">
		<a href="<?php URL::show("Products","new"); ?>">New Product</a>
	</div>
	<div class="col-md-12">
		<table class="table table-striped">
			<thead class="thead-dark">
				<tr>
					<th scope="col">ID</th>
					<th scope="col">Name</th>
					<th scope="col">Price</th>
					<th scope="col">Stock</th>
					<th scope="col" colspan="2"></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($products as $product): ?>
				<tr>
					<td><?php echo $product->getId(); ?></td>
					<td><?php echo $product->getName(); ?></td>
					<td><?php echo $product->getPrice(); ?> RON</td>
					<td><?php echo $product->getStock(); ?></td>
					<td><a href="<?php URL::show("Products","edit",$product->getId()); ?>">Edit</a></td>
					<td><a href="<?php URL::show("Products","delete",$product->getId()); ?>">Delete</a></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>