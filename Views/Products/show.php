<div class="row">
	<?php if($product): ?>
	<div class="col-md-12">
		<h4 class="card-title"><?php echo $product->getName(); ?></h4>
		<p class="card-text"><?php echo $product->getDescription(); ?></p>
		<h4 class="text-right"><?php echo $product->getPrice(); ?> RON</h4>
		<?php CartsController::newAction($product->getId()); ?>
	</div>
	<?php else: ?>
	<div class="col-md-12 alert alert-danger" role="alert">
		Product not found.
	</div>
	<?php endif; ?>
</div>