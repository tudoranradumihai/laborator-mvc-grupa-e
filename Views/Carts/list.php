<?php if(count($cartItems)>0) { ?>
<form method="POST" action="<?php URL::show("Carts","update");?>">
<div class="row">
	<div class="col-md-1 text-center">#</div>
	<div class="col-md-5">Product</div>
	<div class="col-md-2 text-center">Quantity</div>
	<div class="col-md-2 text-center">Product Price</div>
	<div class="col-md-2 text-center">Price</div>
</div>
<?php 
$index = 0;
foreach ($cartItems as $cartItem) { 
$index++;
?>
<div class="row">
	<div class="col-md-1 text-center"><?php echo $index; ?></div>
	<div class="col-md-5"><?php echo $cartItem->getProduct()->getName(); ?></div>
	<div class="col-md-2 text-center"><input type="text" name="items[<?php echo $cartItem->getId() ?>]" class="quantity" value="<?php echo $cartItem->getQuantity(); ?>"></div>
	<div class="col-md-2 text-right"><?php echo $cartItem->getProduct()->getPrice(); ?></div>
	<div class="col-md-2 text-right"><?php echo $cartItem->price; ?></div>
</div>
<?php } ?>
<div class="row">
	<div class="col-md-10 text-right">Total:</div>
	<div class="col-md-2 text-right"><?php echo $priceTotal; ?></div>
</div>
<div class="text-right">
	<input type="submit" class="btn btn-danger" value="Update Cart">
	<a href="" class="btn btn-danger">Checkout</a>
</div>
</form>
<?php } else { ?>
<div class="alert alert-warning" role="alert">
  Your cart is empty! WTF saracule?
</div>
<?php } ?>
<style>
.quantity {
	width:40px;
	height: 40px;
	text-align: center;
}
</style>