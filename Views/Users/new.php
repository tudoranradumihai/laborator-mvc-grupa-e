<div class="row">
    <div class="col-md-12">
        <h1>New User</h1>
        <hr>
    </div>
</div>
<?php
if(array_key_exists("errors", $_SESSION)){
    if($_SESSION["errors"] != ""){
        foreach($_SESSION["errors"] as $error){
            echo '<div class="alert alert-danger" role="alert">'.$error.'</div>';
        }
        unset($_SESSION["errors"]);
    }
}
?>
<form method="POST" action="<?php URL::show("Users", "create"); ?>" id="new-user">
    <div class="form-row">
        <div class="form-group col-md-12">
            <h3>Personal information</h3>
        </div>
        <div class="form-group col-md-6">
            <label for="firstname">Firstname</label>
            <input type="text" id="firstname" name="firstname" class="form-control" placeholder="John">
        </div>
        <div class="form-group col-md-6">
            <label for="lastname">Lastname</label>
            <input type="text" id="lastname" name="lastname" class="form-control" placeholder="Doe">
        </div>
        <div class="form-group col-md-12">
            <h3>Login information</h3>
        </div>
        <div class="form-group col-md-12">
            <label for="email">E-mail Address</label>
            <input type="text" id="email" name="email" class="form-control" placeholder="john.doe@mail.com">
        </div>
        <div class="form-group col-md-6">
            <label for="password">Password</label>
            <input type="password" id="password" name="password" class="form-control" >
        </div>
        <div class="form-group col-md-6">
            <label for="password2">Confirm Password</label>
            <input type="password" id="password2" name="password2" class="form-control" >
        </div>
        <div class="form-group col-md-12">
            <button type="submit" class="btn btn-primary">Create User</button>
        </div>
    </div>
</form>