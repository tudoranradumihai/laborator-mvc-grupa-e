<div class="row justify-content-md-center">
	<div class="col-md-4">
		<h1>Login</h1>
		<form action="<?php URL::show("Users","connect");?>" method="POST">
			<div class="form-group">
				<label for="email">Email address</label>
				<input type="email" class="form-control" id="email" name="email"aria-describedby="emailHelp" placeholder="email@email.com">
				<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
			</div>
			<div class="form-group">
				<label for="password">Password</label>
				<input type="password" class="form-control" id="password" name="password" placeholder="">
			</div>
			<button type="submit" class="btn btn-primary">Login</button>
		</form>
	</div>
</div>