<?php if($status){ ?>
	Hello <?php echo $user->getFirstname()." ".$user->getLastname();?> |
	<a href="<?php URL::show("Users","welcome");?>">My Profile</a> |
	<?php if($user->getAdministrator()){ ?>
	<a href="<?php URL::show("Users","administration");?>">Administration</a> |
	<?php } ?>
	<a href="<?php URL::show("Users","disconnect");?>">Logout</a> |
	<a href="<?php URL::show("Users","edit");?>">Edit Profile</a>
<?php } else { ?>
	<a href="<?php URL::show("Users","login");?>">Login</a> |
	<a href="<?php URL::show("Users","new");?>">Register</a>
<?php } ?>
