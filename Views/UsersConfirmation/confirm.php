<?php if($validation) { ?>
<div class="alert alert-success" role="alert">
<?php echo $message; ?>
</div>
<?php } else { ?>
<div class="alert alert-danger" role="alert">
<?php echo $message; ?>
</div>
<?php } ?>